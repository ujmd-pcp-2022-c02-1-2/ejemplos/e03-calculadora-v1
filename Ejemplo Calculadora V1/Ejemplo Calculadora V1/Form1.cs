﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejemplo_Calculadora_V1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        { 
            // El igual es el operador de asignación.
            // Le asigna el valor o el resultado de la
            // derecha a la variable de la izquierda.
            decimal Sumando1 = 0;
            decimal Sumando2 = 0;
            decimal Resultado = 0;

            Sumando1 = numericUpDown1.Value;
            Sumando2 = numericUpDown2.Value;

            // Acá utilizamos por primera vez el operador de suma
            Resultado = Sumando1 + Sumando2;

            textBox1.Text = Resultado.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            decimal Minuendo = 0;
            decimal Sustraendo = 0;
            decimal Resultado = 0;

            Minuendo = numericUpDown1.Value;
            Sustraendo = numericUpDown2.Value;

            // Acá utilizamos por primera vez el operador de resta
            Resultado = Minuendo - Sustraendo;

            textBox1.Text = Resultado.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            decimal Operando1 = 0;
            decimal Operando2 = 0;
            decimal Resultado = 0;

            Operando1 = numericUpDown1.Value;
            Operando2 = numericUpDown2.Value;

            // Acá utilizamos por primera vez el operador de multiplicación
            Resultado = Operando1 * Operando2;

            textBox1.Text = Resultado.ToString();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            decimal Operando1 = 0;
            decimal Operando2 = 0;
            decimal Resultado = 0;

            Operando1 = numericUpDown1.Value;
            Operando2 = numericUpDown2.Value;

            // Acá utilizamos por primera vez la estructura TRY---CATCH
            // porque la división puede dar errores dependiendo del denominador
            try
            {
                // Acá utilizamos por primera vez el operador de división
                Resultado = Operando1 / Operando2;
                textBox1.Text = Resultado.ToString();
            }
            catch (Exception ex)
            {
                // Si dio error, se cae en el CATCH (captura del error)
                // y mostramos indefinido en el TextBox1
                textBox1.Text = "Indefinido: " + ex.Message;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            decimal Operando1 = 0;
            decimal Operando2 = 0;
            decimal Resultado = 0;

            Operando1 = numericUpDown1.Value;
            Operando2 = numericUpDown2.Value;

            // Acá utilizamos por primera vez la estructura TRY---CATCH
            // porque la división puede dar errores dependiendo del denominador
            try
            {
                // Acá utilizamos por primera vez el operador de módulo (residuo división)
                Resultado = Operando1 % Operando2;
                textBox1.Text = Resultado.ToString();
            }
            catch (Exception ex)
            {
                // Si dio error, se cae en el CATCH (captura del error)
                // y mostramos indefinido en el TextBox1
                textBox1.Text = "Indefinido: " + ex.Message;
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            decimal Operando1 = 0;
            decimal Operando2 = 0;
            decimal Resultado = 0;

            Operando1 = numericUpDown1.Value;
            Operando2 = numericUpDown2.Value;

            // Acá utilizamos por primera vez la estructura TRY---CATCH
            // porque la división puede dar errores dependiendo del denominador
            try
            {
                // Acá utilizamos por primera vez la clase Math para potenciación
                Resultado = (decimal) Math.Pow((double)Operando1, (double)Operando2);
                textBox1.Text = Resultado.ToString();
            }
            catch (Exception ex)
            {
                // Si dio error, se cae en el CATCH (captura del error)
                // y mostramos indefinido en el TextBox1
                textBox1.Text = "Indefinido: " + ex.Message;
            }
        }
    }
}
